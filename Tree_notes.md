# 基础知识
## 树的基本性质
+ $n$ 个节点的树有 $n-1$ 条边
+ 无根树是没有根节点的树，树种节点只有相邻节点无父子节点
+ **树的直径**：树上任意两个节点之间最长的路径
+ **树的中心**：树的直径中间节点，它到 **其它点的最长路径最短**。

## 树的直径
### 两次dfs求树的直径（不能解决有负权边的树）
从任意一个节点搜索，找到最远的节点 $y$，然后从 $y$ 搜最远的节点 $z$， $z->y$ 的路径就是树的直径

```c++
#include<bits/stdc++.h>
using namespace std;
const int N = 1e4 + 10, M = N << 1;
int e[M], ne[M], h[N], w[M], idx, n, m, dist[N];

void add(int a, int b, int c){
    e[idx] = b, w[idx] = c, ne[idx] = h[a], h[a] = idx++;
}

void dfs(int u, int p){
    for(int i = h[u]; ~i; i = ne[i]){
        int j = e[i];
        if(j == p) continue;
        dist[j] = dist[u] + w[i];
        dfs(j, u);
    }
}

int main(){
    ios::sync_with_stdio(0),cin.tie(0), cout.tie(0);
    memset(h, -1, sizeof h);
    cin >> n;
    for(int i = 1; i < n; i++){
        int a, b, c;
        cin >> a >> b >> c;
        add(a, b, c), add(b, a, c);
    }
    dfs(1, -1);
    memset(dist, 0, sizeof dist);
    int mx = 0, idx = 0;
    for(int i = 1; i <= n; i++)
        if(dist[i] >= mx){
            mx = dist[i];
            idx = i;
        }
    memset(dist, 0, sizeof dist);
    dfs(idx, -1);
    int ans = 0;
    for(int i = 1; i <= n; i++)
        ans = max(ans, dist[i]);
    cout << ans << endl;
    return 0;
}
```

## 树的重心
树中某个节点的每个子节点的子树大小（节点数目） **最大值最小**的那个节点被称为 **树的重心**，可能会有 $1-2$ 个重心。

### 重心的性质

+ 当重心为根节点时，它底下每个子树的大小 **不大于整棵树大小的一半**;
+ 重心到其它所有节点的 **距离和最小**。

### dfs求树的重心

```c++
#include<bits/stdc++.h>
using namespace std;
const int N = 1e5 + 10, M = N << 1;

int h[N], e[M], ne[M], w[M], idx, n, fa[N], sz[N];

void add(int a, int b){
    e[idx] = b, ne[idx] = h[a], h[a] = idx++;
}

void dfs(int u, int p){
	sz[u] = 1;
	for(int i = h[u]; ~i; i = ne[i]){
		int j = e[i];
		if(j == fa[u]) continue;
		fa[j] = u;
		dfs(j, u);
		sz[u] += sz[j];
	}
}

int main(){
	ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);
    memset(h, -1, sizeof h);
    cin >> n;
    for(int i = 0; i < n - 1; i++){
    	int a, b;
    	cin >> a >> b;
    	add(a, b), add(b, a);
    }
    dfs(1, -1);
    int ans = 0, mx = 1e9, idx = 0;
    for(int u = 1; u <= n; u++){
    	int t = 0;
    	for(int i = h[u]; ~i; i = ne[i]){
    		int v = e[i];
    		if(v == fa[u])
    			t = max(t, n - sz[u]);
    		else
    			t = max(t, sz[v]);
    	}
    	if(t < mx){
    		mx = t;
    		idx = u;
    		ans = t;
    	}
    }
    cout << ans << endl;
    return 0;
}
```

## DFS、BFS序

+ DFS序：从根节点开始 dfs 搜索过程中依次记录经过的点所生成的序列。
+ BFS序：从根节点开始 bfs 搜索过程中依次记录经过的点所生成的序列。

## 树的最近公公祖先，LCA 问题

对于两个节点 $u、v$， 找到一个树深度最大的 $x$ ，$x$ 是 $u、v$ 的祖先。多个节点的 LCA 问题可以转换成多次求两个节点 LCA 的形式。
### 倍增法求解LCA

+ 先计算出两个节点 $u,v$ 的深度；
+ 将 $u,v$ 调整到同一深度
+ 两个节点一起逐级向上跳，直到两节点相等

$fa[x][y]$ 代表从 $x$ 向上跳 $2^{y-1}$ 步的祖先是谁。  

倍增思想递推
```c++
fa[x][y] = fa[fa[x][y - 1]][y - 1];
```

实现代码:

```c++
int dep[N], fa[N][Lg];

void dfs(int u, int p){
	for(int i = h[u]; ~i; i = ne[i]){
		int j = e[i];
		if(j == p) continue;
		dep[j] = dep[u] + 1;
		fa[j][0] = u;
		dfs(j, u);
	}
}

void init(){
	for(int i = 1; i < Lg; i++)		// 先循环跳的次数
		for(int j = 1; j <= n; j++)		// 再循环节点个数
			if(fa[j][i - 1])
				fa[j][i] = fa[fa[j][i - 1]][i - 1];
}

int lca(int a, int b){		// 求点 a 和 点 b 的最近公共祖先
	if(dep[a] < dep[b])
		swap(a, b);
	int d = dep[a] - dep[b];		// 深度大的是 a
	for(int i = 0; i < Lg && d; i++, d /= 2){
		if(d & 1)
			a = fa[a][i];
	}
	if(a == b) return a;
	for(int i = Lg - 1; i >= 0; i--)
		if(fa[a][i] != fa[b][i])
			a = fa[a][i], b = fa[b][i];
	return fa[a][0];
}
```


## 重链剖分
### 定义
**重子节点：**子节点中子树最大的子节点，多个取其一，没有子节点就没有。  
**轻子节点：**剩余所有子节点，重子节点以外节点。
**重边：**节点到重子节点的边
**重链：**若干首尾相连的重边，把落单节点也酸橙重链，那么树被剖分成若干条重链

### 性质
1. **树上每个节点都属于且仅属于一条重链**
2. 重链开头的结点不一定是重子节点（因为重边是对于每一个结点都有定义的）
3. 所有的重链将整棵树 **完全剖分**
4. 在剖分时 **重边优先遍历**，最后树的 DFN 序上，重链内的 DFN 序是连续的。按 DFN 排序后的序列即为剖分后的链。
5. 一颗子树内 DFN 序连续
6. 向下经过一条 **轻边**，子树大小至少除二。
7. 因此，对于树上的任意一条路径，把它拆分成从 $LCA$ 分别向两边往下走，分别最多走 $O(logn)$ 次，因此，树上的每条路径都可以被拆分成不超过 $O(logn)$ 条重链。

+ 重链剖分可以将树上的任意一条路径划分成不超过 $O(logn)$ 条连续的链，
每条链上的点深度互不相同（即是自底向上的一条链，链上所有点的 LCA 为链的一个端点）。

+ 重链剖分还能保证划分出的每条链上的节点 DFS 序连续，
因此可以方便地用一些维护序列的数据结构（如线段树）来维护树上路径的信息。
    + 修改 **树上两点之间的路径上所有点**的值。
    + 查询 **树上两点之间的路径上** 节点权值的 **和/极值/其它（在序列上可以用数据结构维护，便于合并的信息）**。

### 模板
```c++
int fa[N], dep[N], sz[N], hson[N], top[N], dfn[N], rank[N], tot;
void dfs1(int u, int p){        // 获取 fa[u],dep[u],sz[u],hson[u]
    hson[u] = -1;       // dep[root] 务必为0
    sz[u] = 1;
    for(int i = h[u]; ~i; i = ne[i]){
        int v = e[i];
        if(v == p) continue;
        dep[v] = dep[u] + 1;
        fa[v] = u;
        dfs1(v, u);
        sz[u] += sz[v];
        if(hson[u] == -1 || sz[v] > sz[hson[u]]) hson[u] = v;
    }
}

void dfs2(int u, int tp){
    top[u] = tp;
    dfn[u] = ++ tot;
    rank[tot] = u;
    if(hson[u] == -1) return;   // 没有重节点 等价 是叶子节点
    dfs2(hson[u], u);   // 优先对重儿子dfs，保证同一重链点 DFS 序连续
    for(int i = h[u]; ~i; i = ne[i]){
        int v = e[i];
        if(v == hson[u] || v == fa[u]) continue;
        dfs2(v, v); // 不是重节点，top[v] = v;
    }
}
```

### 应用
#### 路径上维护
例：维护两点路径权值和
+ 链上的 DFS 序是连续的，可以使用线段树、树状数组维护。
+ 每次选择深度较大的链往上跳，直到两点在同一条链上。
+ 同样的跳链结构适用于维护、统计路径上的其他信息。
```c++
int path_sum(int u, int v){
    int res = 0;
    while(top[u] != top[v]){
        if(dep[top[u]] < dep[top[v]])
            swap(u, v);
        res += ... // res += u 到 top[u] 的路径和
        u = fa[top[u]];
    }
    res += ... // res += u 到 v 上的路径和
}
```

#### 子树维护
由于子树 DFS 序是连续的，很容易对整个子树进行操作。记录 `in[u], out[u]` 即可。

#### 求最近公共祖先(LCA)
+ 不断向上跳重链，当跳到同一条重链上时，深度较小的结点即为 LCA。
+ 向上跳重链时需要先跳所在重链顶端深度较大的那个
```c++
int lca(int u, int v){
    while(top[u] != top[v]){
        if(dep[top[u]] > dep[top[v]])
            u = fa[top[u]];
        else
            v = fa[top[v]];
    }
    return dep[u] > dep[v] ? v : u;
}
```

